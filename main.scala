/**
 * http://javatoscala.com/
 * http://tomp2p.net/doc/quick/
 *
 * sbt "run-main ExampleSimple 1 test.me 192.168.1.1" | grep ExampleSimple
 * sbt "run-main ExampleSimple 2 test.me" | grep ExampleSimple
 *
 */

import java.io.IOException
import net.tomp2p.futures.FutureDHT
import net.tomp2p.futures.FutureBootstrap
import net.tomp2p.p2p.Peer
import net.tomp2p.p2p.PeerMaker
import net.tomp2p.peers.Number160
import net.tomp2p.storage.Data
import ExampleSimple._
//remove if not needed
import scala.collection.JavaConversions._

object ExampleSimple {

  def main(args: Array[String]) {
    println("ExampleSimple: hello world")

    val dns = new ExampleSimple(java.lang.Integer.parseInt(args(0)))

    println("ExampleSimple: dns initialized")

    println("ExampleSimple: args" + args.mkString(", "))
    if (args.length == 3) {
      println("ExampleSimple: store")
      dns.store(args(1), args(2))
    }
    if (args.length == 2) {
      println("ExampleSimple: Name:" + args(1) + " IP:" + dns.get(args(1)))
    }
  }
}

class ExampleSimple(peerId: Int) {
  println("ExampleSimple: init class")

  private val peer = new PeerMaker(Number160.createHash(peerId)).setPorts(4000 + peerId)
    .makeAndListen()
  println("ExampleSimple: 1")

  val fb = peer.bootstrap().setBroadcast().setPorts(4001).start()
  println("ExampleSimple: 2")

  fb.awaitUninterruptibly()
  println("ExampleSimple: 3")

  if (fb.getBootstrapTo != null) {
    peer.discover().setPeerAddress(fb.getBootstrapTo.iterator().next())
      .start()
      .awaitUninterruptibly()
  }
  println("ExampleSimple: 4")

  // キー名文字列を受け取ってバリュー文字列を返す
  def get(name: String): String = {
    println("ExampleSimple: get from DHT")

    val futureDHT = peer.get(Number160.createHash(name)).start()
    futureDHT.awaitUninterruptibly()
    if (futureDHT.isSuccess) {
      println("ExampleSimple: futureDHT.isSuccess")
      return futureDHT.getData.getObject.toString
    }
    "not found"
  }

  def store(name: String, ip: String) {
    println("ExampleSimple: store data")
    peer.put(Number160.createHash(name)).setData(new Data(ip))
      .start()
      .awaitUninterruptibly()
  }
}
