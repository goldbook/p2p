name := "upnp"

version := "1.0"

// scalaVersion := "2.11.1"

resolvers in ThisBuild ++= Seq(
  "tomp2p" at "http://tomp2p.net/dev/mvn/"
  , "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
)

libraryDependencies ++= Seq(
  "net.tomp2p" % "TomP2P" % "4.4"
  , "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"
  , "org.slf4j" % "slf4j-api" % "1.7.1"
  , "org.slf4j" % "log4j-over-slf4j" % "1.7.1"
  , "ch.qos.logback" % "logback-classic" % "1.0.3"
)
